#<plugin>
#                <groupId>org.apache.maven.plugins</groupId>
#                <artifactId>maven-release-plugin</artifactId>
#                <version>2.3.2</version>
#                <configuration>
#                    <tagNameFormat>v@{project.version}</tagNameFormat>
#                    <pushChanges>false</pushChanges>
#                    <localCheckout>true</localCheckout>
#                </configuration>
#            </plugin>

#Few explanation here :

#    tagNameFormat is here to change the default tag name (which is ${project.artifactId}-${project.version}) to a better one.
#    pushChanges set to false tells maven-release-plugin not to push changes (this will become useful)
#    localCheckout set to true tells maven-release-plugin to clone from local repository (not distant). This is especially useful here because we didn't push anything (so not setting this option would result in a failure).

#First create a release branch from develop
git checkout -b release/v0.1 develop

#Then run the maven release stuff.

mvn release:prepare               # change the pom, commit and tag version, and
                                    # re-change pom (by incrementing SNAPSHOT version)
mvn release:perform               # get the tagged version, compile and deploy

#And the real fun begins.

git checkout develop              # get back to the develop branch
git merge --no-ff release/v0.1    # merge the version back into develop
git checkout master               # go to the master branch
git merge --no-ff release/v0.1~1  # merge the version back into master but
                                    # the tagged version instead of the release/v0.1 HEAD
git branch -D release/v0.1        # Removing the release branch
git push --all && git push --tags # Finally push everything

#The real magic here is the git merge --no-ff release/v0.1~1 which will merge into master the commit before the HEAD of the branch release/v0.1.

#The next step would be to create a helper script that automates this and verify that the pom.xml has the right configuration options.
