# How to perform a release with git & maven following the git flow conventions
# ----------------------------------------------------------------------------
# Finding the next version: you can see the next version by looking at the 
#	version element in "pom.xml" and lopping off "-SNAPSHOT". To illustrate, 
#	if the pom's version read "0.0.2-SNAPSHOT", the following instructions would 
#	perform the release for version "0.0.2" and increment the development version 
#	of each project to "0.0.3-SNAPSHOT".

# branch from develop to a new release branch
git checkout develop
git checkout -b release/v0.0.2

# perform a maven release, which will tag this branch and deploy artifacts to 
#	nexus (currently: http://10.202.4.52:8081/nexus )
mvn release:prepare
mvn release:perform

# merge the version changes back into develop so that folks are working against 
#	the new release ("0.0.3-SNAPSHOT", in this case)
git checkout develop
git merge --no-ff release/v0.0.2

# housekeeping -- rewind the release branch by one commit to fix its version at "0.0.2"
#	excuse the force push, it's because maven will have already pushed '0.0.3-SNAPSHOT'
#	to origin with this branch, and I don't want that version (or a diverging revert commit)
#	in the release or master branches.
git checkout release/v0.0.2
git reset --hard HEAD~1
git push --force origin release/v0.0.2
git checkout develop

# finally, if & when the code gets deployed to production
git checkout master
git merge --no-ff release/v0.0.2
git branch -d release/v0.0.2
